## Files

`config.py`: Loads config from config.json and returns config 

`download_h5_list.py` : Fetches list of all the GEDI L2A files for each TCF property and stores csv file for each property

`download_h5_subset.py`: Downloads the H5 files fetched from previous step and subset for TCF properties

`merge_l2a_files`: Combines all the l2a files from each properties and combines into a single csv for modeling

`preprocess.py`: This file contains utlitity function for preprocessing data for modeling

`standardize_rasters_io`: A helper file to load rasters from TIF file and access raster data for input lat long 

`combine_inventory_rasters` :  Merge all raster data with TCF manual surveying sites 
