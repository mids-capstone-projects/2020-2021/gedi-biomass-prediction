import os
import h5py
import numpy as np
import pandas as pd
from shapely.geometry import Polygon, MultiPolygon, shape, Point
import geopandas as gpd
import glob

def merge_l2a(tcf_geojson_fpath, l2a_csv_dirname, output_l2a_file):
    """
    Merges all the L2A substted files from all the properties into single dataframe
    and stores the dataframe in the output_l2a_file filepath.
    """

    prop = gpd.read_file(tcf_geojson_fpath)
    prop = prop.explode()
    prop=prop[['Project','geometry']].reset_index()

    # get all L2A CSV files (subsetted)
    l2a_files = glob.glob(l2a_csv_dirname +'/*/*.csv')

    l2a = []
    for filename in l2a_files:
        l2a.append(pd.read_csv(filename))

    # Concatenate all data into one DataFrame
    L2A = pd.concat(l2a, ignore_index=True)

    L2A = gpd.GeoDataFrame(L2A.reset_index())
    # Convert lat and long to geometric type point
    L2A['geometry'] = L2A.apply(lambda row: Point(row.lon_lowestmode, row.lat_lowestmode), axis=1)

    prop_list = prop.Project.unique()

    all_df = []
    total = 0
    # Spatial inner join between L2A dataset and TCF Properties
    for i in range(len(prop_list)):
        tcf_property = prop[prop['Project'] == prop_list[i]]
        joinDF = gpd.sjoin(L2A.set_crs("EPSG:4326"), tcf_property.set_crs("EPSG:4326"), how="inner", op="within")
        total = total + len(joinDF)
        all_df.append(joinDF)
        print('{}, Rows: {}'.format(prop_list[i], len(joinDF)))

    print(f'Total L2A Row Count: {L2A.shape[0]}\nLabeled L2A Count: {total}')

    all_df = pd.concat(all_df, ignore_index=True)

    all_df = all_df[['elev_highestreturn', 'elev_lowestmode',
       'lat_highestreturn', 'lat_lowestmode', 'lon_highestreturn',
       'lon_lowestmode', 'rh_05', 'rh_10', 'rh_15', 'rh_20', 'rh_50', 'rh_95',
       'rh_97', 'rh_98', 'rh_99', 'rh_100', 'rh_101', 'shot_number',
       'geometry','Project_left']].rename(columns={"lat_lowestmode": "Latitude", "lon_lowestmode": "Longitude", "Project_left": "Project"})

    all_df.to_csv(output_l2a_file, index = False)

    return all_df
