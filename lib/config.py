import json

def load_property(json_config, property_json_key):
    if property_json_key in json_config:
        return json_config[property_json_key]
    else:
        print("Missing parameter in config.json:", property_json_key)
        exit(1)

def load_config(config_file):
    with open(config_file) as f:
        json_conf = json.load(f)

    conf = dict()
    conf['GEDI_USERNAME'] = load_property(json_conf, 'GEDI_USERNAME')
    conf['GEDI_PASSWORD'] = load_property(json_conf, 'GEDI_PASSWORD')

    conf['OUTPUT_DIR'] = load_property(json_conf, 'OUTPUT_DIR')

    conf['TCF_PROPERTY_GEOJSON_FILE'] = load_property(json_conf, 'TCF_PROPERTY_GEOJSON_FILE')
    conf['GEDI_L2A_KEYS'] = load_property(json_conf, 'GEDI_L2A_KEYS')
    conf['RASTERS'] = load_property(json_conf, 'RASTERS')
    conf['INVENTORY_DATA_DIR'] = load_property(json_conf, 'INVENTORY_DATA_DIR')
    conf['L2A_TCF_OUTPATH'] = load_property(json_conf, 'L2A_TCF_OUTPATH')

    conf['H5_SOURCE_DIR'] = "./tmp/h5/"
    conf['H5_CSV_DIR'] = "./tmp/h5csv/"
    conf['H5_CSV_SUBSET_DIR'] = conf['OUTPUT_DIR']
    conf['TCF_PROPERTY_PICKLE_FILE'] = './tmp/prop_names.pickle'
    conf['TCF_PROPERTY_CSV_DIR'] = './tmp/listings/'
    return conf
