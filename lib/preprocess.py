import pandas as pd
from pyproj import Proj, Transformer
from math import sqrt
import numpy as np
import os

def gediL2A_data_cleaning(gedi_l2a, rh_cols):
    # Remove rows with RH values outside the middle 96% (top and bottom 2%).
    gedi_l2a['is_outlier'] = 0
    for rh_field in rh_cols:   
        gedi_l2a.loc[gedi_l2a[rh_field] < gedi_l2a[rh_field].quantile(.02), 'is_outlier'] = 1
        gedi_l2a.loc[gedi_l2a[rh_field] > gedi_l2a[rh_field].quantile(.98), 'is_outlier'] = 1

    gedi_l2a = gedi_l2a[gedi_l2a['is_outlier'] == 0]
    return gedi_l2a

def gediL2A_filter_column(gedi_l2a_df, rh_cols):
    cols = gedi_l2a_df.columns
    for c in list(cols):
        if not (c in rh_cols or c.startswith('eqdist')):
            gedi_l2a_df = gedi_l2a_df.drop(c, axis=1)
    return gedi_l2a_df

def get_equidistant_projection(df):
    # eqdist conic projection is used for distance-based interpolation, create those cols.
    # Must have Longitude and Latitude as columns in input df
    USA_equidistant_conic_projection_str = "+proj=eqdc +lat_1=24.9493 +lat_2=49.5904" # Equidistant conic projection, bounding to USA
    projector = Proj(USA_equidistant_conic_projection_str)
    
    df['eqdist_conic_x'], df['eqdist_conic_y'] = projector(df['Longitude'].to_numpy(),df['Latitude'].to_numpy())
        
    return df

def tcf_format_columns(categoricals, columns_of_interest, inv_df):
    # Some do not get read in as categories, need to convert them.
    print('Len Before Format :', len(inv_df.columns))
    one_hot_categories = [c for c in categoricals if c in columns_of_interest]
    for c in categoricals:
        inv_df[c] = inv_df[c].astype('category')
        
    for c in one_hot_categories:
        one_hot = pd.get_dummies(inv_df[c], prefix=c)
        inv_df = inv_df.drop(c, axis = 1) # Drop column as it is now encoded
        inv_df = inv_df.join(one_hot)

    print('Len After Format :', len(inv_df.columns))
    return inv_df

def tcf_filter_columns(columns_of_interest, inv_df):
    print('Len Before Filter :', len(inv_df.columns))
    for c in list(inv_df.columns):
        if not (any(c.startswith(ci) for ci in columns_of_interest) or c.startswith('eqdist')):
            inv_df = inv_df.drop(c, axis=1)
    print('Len After Filter :', len(inv_df.columns))
    return inv_df

def filter_data(inv_df, gedi_l2a, prop_name, year):
    inv_df = inv_df.loc[inv_df.Year == year]
    
    # bring property name and project in same format
    inv_df.Property_ = inv_df.Property_.str.replace('_', ' ')
    
    inv_prop_df = inv_df.loc[inv_df.Property_ == prop_name]
    gedi_l2a = gedi_l2a.loc[gedi_l2a.Project == prop_name]
    print('Before: ', len(gedi_l2a))
    gedi_l2a = gedi_l2a.loc[gedi_l2a.rh_101 != 0.00]
    print('After: ', len(gedi_l2a))
    
    # remove zeros from target variable
    inv_prop_df = inv_prop_df[inv_prop_df['Jenkins_Total_MTCO2e'] != 0]
    
    return inv_prop_df, gedi_l2a

def get_tcf_train_test_split(inv_prop_df, split_ratio=0.85, seed=123):
    shuffled = inv_prop_df.sample(frac=1, random_state=seed)
    target_points = shuffled.iloc[0:round((1-split_ratio)*len(inv_prop_df)),:]
    kept_points = shuffled.iloc[round((1-split_ratio)*len(inv_prop_df)):,:]
    print(f"Kept {len(kept_points)}, interpolating {len(target_points)}")
    return kept_points, target_points


def format_target_dataframe(target_df, kept_df, gedi_l2a, k=5):
    """
    Format Dataframe for use with MLP. Returns the numpy array "X" needed for sklearn's MLPRegressor.
    
    This is the builder for the target df, so it chooses close points from the kept data.
    The dataframes should be passed in with only regression-relevant columns in them (vars and eqdist projection coords).
    Conic projection columns must be named eqdist_conic_x and eqdist_conic_y.
    
    Input:
       target_df: DataFrame of target points, with only x/y and columns of interest. This should only have x/y and raster data.
       kept_df: DataFrame of input points, with only x/y and columns of interest.
       gedi_l2a: DataFrame of GEDI L2A data product measurements, with eqdist_conic_x, eqdist_conic_y, and relative height metrics defined.

    Returns: A numpy array, X for the regressor prediction.
    1. Convert to numpy array
    2. Add in data from k nearest points.
    """
    
    # Locate x and y variables within each dataframe.
    kept_x_idx = kept_df.columns.get_loc('eqdist_conic_x')
    kept_y_idx = kept_df.columns.get_loc('eqdist_conic_y')
    kept_mat = kept_df.to_numpy().astype('float')

    target_x_idx = target_df.columns.get_loc('eqdist_conic_x')
    target_y_idx = target_df.columns.get_loc('eqdist_conic_y')
    target_mat = target_df.to_numpy().astype('float')

    gedi_l2a_x_idx = gedi_l2a.columns.get_loc('eqdist_conic_x')
    gedi_l2a_y_idx = gedi_l2a.columns.get_loc('eqdist_conic_y')
    gedi_l2a_mat = gedi_l2a.to_numpy().astype('float')

    num_cols = kept_mat.shape[1] - 2 # The number of non-coordinate variables in the matrices.
    num_cols_gedi_l2a = gedi_l2a_mat.shape[1] - 2

    # Move x and y columns to front for convenience    
    kept_mat[:, [kept_x_idx, 0]] = kept_mat[:, [0, kept_x_idx]]
    kept_mat[:, [kept_y_idx, 1]] = kept_mat[:, [1, kept_y_idx]]
    
    target_mat[:, [target_x_idx, 0]] = target_mat[:, [0, target_x_idx]]
    target_mat[:, [target_y_idx, 1]] = target_mat[:, [1, target_y_idx]]
    
    gedi_l2a_mat[:, [gedi_l2a_x_idx, 0]] = gedi_l2a_mat[:, [0, gedi_l2a_x_idx]]
    gedi_l2a_mat[:, [gedi_l2a_y_idx, 1]] = gedi_l2a_mat[:, [1, gedi_l2a_y_idx]]

    # Compute nearest k points for each, create new columns.
    near_point_vars = []
    for i in range(len(target_mat)):
        # Find k nearest surveying plot points from TCF inventory.
        distances = np.sqrt(np.sum((target_mat[i,:2] - kept_mat[:,:2])**2, axis=1))    
        idx = np.argpartition(distances, k)[:k]
        near_distances = np.partition(distances, k)[:k]
        near_points = kept_mat[idx, 2:]
        near_distances = near_distances.reshape(k,1)
        near_points = np.hstack((near_points, near_distances))
        near_points = near_points[np.argsort(near_points[:,-1])]

        # Find k nearest GEDI L2A points.
        distances_gedi_l2a = np.sqrt(np.sum((target_mat[i,:2] - gedi_l2a_mat[:,:2])**2, axis=1))    
        idx_gedi_l2a = np.argpartition(distances_gedi_l2a, k)[:k]
        near_distances_gedi_l2a = np.partition(distances_gedi_l2a, k)[:k]
        near_points_gedi_l2a = gedi_l2a_mat[idx_gedi_l2a, 2:]
        near_distances_gedi_l2a = near_distances_gedi_l2a.reshape(k,1)
        near_points_gedi_l2a = gedi_l2a_mat[idx_gedi_l2a, 2:]
        near_points_gedi_l2a = np.hstack((near_points_gedi_l2a, near_distances_gedi_l2a))
        near_points_gedi_l2a = near_points_gedi_l2a[np.argsort(near_points_gedi_l2a[:,-1])]
        
        # Paste together the GEDI variables and the data from TCF surveying to make a new row in the tabular
        # form of this data.
        near_points = np.hstack((near_points, near_points_gedi_l2a))
        near_points = near_points.reshape((1, (num_cols+num_cols_gedi_l2a+2)*k))
        near_point_vars.append(near_points) # insert new row that we just created into the output
        pass

    near_point_vars = np.concatenate(near_point_vars)
    return near_point_vars

def format_kept_dataframe(kept_df, gedi_l2a, k=5):
    # Locate x and y variables within each dataframe.
    x_idx = kept_df.columns.get_loc('eqdist_conic_x')
    y_idx = kept_df.columns.get_loc('eqdist_conic_y')
    mat = kept_df.to_numpy().astype('float')
    
    gedi_l2a_x_idx = gedi_l2a.columns.get_loc('eqdist_conic_x')
    gedi_l2a_y_idx = gedi_l2a.columns.get_loc('eqdist_conic_y')
    gedi_l2a_mat = gedi_l2a.to_numpy().astype('float')
    
    num_cols = mat.shape[1] - 2 # The number of non-coordinate variables in the matrix.
    num_cols_gedi_l2a = gedi_l2a_mat.shape[1] - 2
    
    # Move x and y columns to front for convenience
    mat[:, [x_idx, 0]] = mat[:, [0, x_idx]]
    mat[:, [y_idx, 1]] = mat[:, [1, y_idx]]

    gedi_l2a_mat[:, [gedi_l2a_x_idx, 0]] = gedi_l2a_mat[:, [0, gedi_l2a_x_idx]]
    gedi_l2a_mat[:, [gedi_l2a_y_idx, 1]] = gedi_l2a_mat[:, [1, gedi_l2a_y_idx]]
    
    # Compute nearest k points for each, create new columns.
    near_point_vars = []
    for i in range(len(mat)):
        # Find k nearest input surveying plots (TCF survey plots).
        distances = np.sqrt(np.sum((mat[i,:2] - mat[:,:2])**2, axis=1))    
        idx = np.argpartition(distances, k+1)[:k+1]
        near_distances = np.partition(distances, k+1)[:k+1]
        idx = idx[np.where(idx != i)] # Take out the top one. Recall, this function formats training data, so the top 1 is the point itself.
        near_distances = near_distances[np.where(near_distances != 0.0)] # same thing as above, for the closest distance (the point itself)
        near_points = mat[idx, 2:]
        near_distances = near_distances.reshape(k,1)
        near_points = np.hstack((near_points, near_distances))
        near_points = near_points[np.argsort(near_points[:,-1])]
        
        # Find k nearest GEDI L2A points.
        distances_gedi_l2a = np.sqrt(np.sum((mat[i,:2] - gedi_l2a_mat[:,:2])**2, axis=1))    
        idx_gedi_l2a = np.argpartition(distances_gedi_l2a, k)[:k]
        near_distances_gedi_l2a = np.partition(distances_gedi_l2a, k)[:k]
        near_distances_gedi_l2a = near_distances_gedi_l2a.reshape(k,1)
        near_points_gedi_l2a = gedi_l2a_mat[idx_gedi_l2a, 2:]
        near_points_gedi_l2a = np.hstack((near_points_gedi_l2a, near_distances_gedi_l2a))
        near_points_gedi_l2a = near_points_gedi_l2a[np.argsort(near_points_gedi_l2a[:,-1])]
        
        # Paste together the GEDI variables and the data from TCF surveying to make a new row in the tabular
        # form of this data.
        near_points = np.hstack((near_points, near_points_gedi_l2a))
        near_points = near_points.reshape((1, (num_cols+num_cols_gedi_l2a+2)*k))
        near_point_vars.append(near_points) # insert new row that we just created into the output

    near_point_vars = np.concatenate(near_point_vars)
    return near_point_vars

def get_per_nn_columns(tcf_columns, rh_cols):    
    _tcf_columns = tcf_columns.copy()
    _rh_cols = rh_cols.copy()
    # remove these
    _tcf_columns.remove('eqdist_conic_x')
    _tcf_columns.remove('eqdist_conic_y')
    # add the distances
    _tcf_columns.append('tcf_distances')
    _rh_cols.append('gedi_distances')
    
    return _tcf_columns + _rh_cols

def get_all_columns(per_nn_column, k=5):
    all_nn_columns = []

    for i in range(1, k+1):
        for col_name in per_nn_column:
            all_nn_columns.append(str(col_name+"_nn_"+str(i)))
            
    return all_nn_columns

def prepare_final_df(kept_points, target_points, gedi_l2a, rh_cols, k=5):
    kept_X = format_kept_dataframe(kept_points, gedi_l2a, k)
    target_X = format_target_dataframe(target_points, kept_points, gedi_l2a, k)
    
    tcf_columns = list(kept_points.columns)
    per_nn_columns = get_per_nn_columns(tcf_columns, rh_cols)
    all_nn_columns = get_all_columns(per_nn_columns, k)

    train_df = pd.DataFrame(kept_X, columns = all_nn_columns)
    test_df = pd.DataFrame(target_X, columns = all_nn_columns)
    
    # y is the ground truth: Jenkins_Total_MTCO2e
    train_df.insert(0, 'y', list(kept_points['Jenkins_Total_MTCO2e']))
    test_df.insert(0, 'y', list(target_points['Jenkins_Total_MTCO2e']))
    print(train_df.shape, test_df.shape)
    return train_df, test_df