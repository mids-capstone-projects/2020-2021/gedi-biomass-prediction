import os
import pandas as pd
import glob
import geopandas as gpd
import h5py
import numpy as np
from shapely.geometry import Point
import pickle
import signal
import warnings
warnings.filterwarnings("ignore")

def beam_to_df(bn, hf):
    """
    Given a beam name and an l2b h5 file object,
    return a dictionary of np arrays
    """
    beam = hf[bn]
    fields_dict = dict()
    for k in beam.keys():
        if k=='rx_1gaussfit':
            continue
        try:
            fields_dict[k] = beam[k][()]
        except:
            # If the above failed, it's nested - pull out subkeys
            for subkey in beam[k]:
                if subkey=='ancillary':
                    continue
                #print('Hello ', k, " ", subkey)
                fields_dict[f"{k}/{subkey}"] = beam[k][subkey][()]
    return fields_dict

## create a set of unique file names
def download_gedifiles(df_tcf, h5_csv, prop_name, username, password, h5_output_dir, csv_output_sub_dir, csv_output_dir, wanted_keys):
    df_h5 = pd.read_csv(h5_csv)

    for all_links in list(df_h5['links']):
        for h5_file_url in all_links.strip('][').split(', '):
            print(h5_file_url)
            wget_command = 'wget --http-user={} --http-password={} -P {} {}'.format(
                username, password, h5_output_dir, h5_file_url)

            h5_filename = os.path.basename(h5_file_url).replace("'", "")
            output_h5_file = os.path.join(h5_output_dir, h5_filename)
            output_h5_csv = os.path.join(csv_output_dir, h5_filename+".csv")
            output_h5_csv_sub = os.path.join(csv_output_sub_dir, prop_name, h5_filename + 'sub.csv')
            try:
                if os.path.exists(output_h5_csv_sub):
                    print('Already Col Subsetted', output_h5_csv_sub)
                    continue

                print("Checking: ", output_h5_file)
                if not os.path.exists(output_h5_file):
                    print('Downloading: ', h5_file_url)
                    exit_code = os.system(wget_command)
                    if exit_code == signal.SIGINT:
                        print("\nExiting (Manual Stop)")
                        exit(exit_code)
                    if exit_code != 0:
                        print(f"WGET Error Code: {exit_code}")
                        continue
                else:
                    print('Already downloaded', output_h5_file)

                # Subset Columns
                if os.path.exists(output_h5_csv):
                    print('Already Column Subsetted', output_h5_csv)
                    rh_df = pd.read_csv(output_h5_csv)
                else:
                    print('Subsetting ', output_h5_csv)
                    rh_df = subset(output_h5_file, output_h5_csv, wanted_keys)

                print("Row Subsetting")
                County_DFs(df_tcf[df_tcf['Project'] == prop_name], rh_df, output_h5_csv_sub)

                print('Deleting: ', output_h5_file)
                os.remove(output_h5_file)
                os.remove(output_h5_csv)

            except Exception as e:
                print(e.with_traceback())
                print('Error:', h5_file_url)


def subset(input_h5_file, output_csv, wanted_keys):

    h5 = h5py.File(input_h5_file, 'r')

    # dont process if file already created, good if running on same directory
    if os.path.exists(output_csv):
        print(f"File Already Processed: {os.path.basename(input_h5_file)}")
        return

    print(f"Processing file: {os.path.basename(input_h5_file)}")

    beamNames = [k for k in h5.keys() if k.startswith('BEAM')]

    data = dict()
    for bn in beamNames:
        beam_data = beam_to_df(bn, h5)
        for k in beam_data:
            if k in wanted_keys:
                if k.endswith('rh'):
                    if "rh_05" not in data:
                        data[k + "_05"] = beam_data[k][:,4]
                        data[k + "_10"] = beam_data[k][:,9]
                        data[k + "_15"] = beam_data[k][:,14]
                        data[k + "_20"] = beam_data[k][:,19]
                        data[k + "_50"] = beam_data[k][:,49]
                        data[k + "_95"] = beam_data[k][:,94]
                        data[k + "_97"] = beam_data[k][:,96]
                        data[k + "_98"] = beam_data[k][:,97]
                        data[k + "_99"] = beam_data[k][:,98]
                        data[k + "_100"] = beam_data[k][:,99]
                        data[k + "_101"] = beam_data[k][:,100]
                    else:
                        data[k + "_05"] = np.append(data[k + "_05"], beam_data[k][:,4])
                        data[k + "_10"] = np.append(data[k + "_10"], beam_data[k][:,9])
                        data[k + "_15"] = np.append(data[k + "_15"], beam_data[k][:,14])
                        data[k + "_20"] = np.append(data[k + "_20"], beam_data[k][:,19])
                        data[k + "_50"] = np.append(data[k + "_50"], beam_data[k][:,49])
                        data[k + "_95"] = np.append(data[k + "_95"], beam_data[k][:,94])
                        data[k + "_97"] = np.append(data[k + "_97"], beam_data[k][:,96])
                        data[k + "_98"] = np.append(data[k + "_98"], beam_data[k][:,97])
                        data[k + "_99"] = np.append(data[k + "_99"], beam_data[k][:,98])
                        data[k + "_100"] = np.append(data[k + "_100"], beam_data[k][:,99])
                        data[k + "_101"] = np.append(data[k + "_101"], beam_data[k][:,100])
                else:
                    if k not in data:
                        data[k] = beam_data[k]
                    else:
                        data[k] = np.append(data[k], beam_data[k])

    wrong_shape = [k for k in data if data[k].shape != data['lat_lowestmode'].shape]
    for bad_k in wrong_shape:
        del data[bad_k]
    rh_df = pd.DataFrame(data=data)
    rh_df.to_csv(output_csv, index=False)

    print('File subset completed', output_csv)
    return rh_df


def download(tcf_geojson_fpath, input_csv_boundaries_dir, h5_output_dir, csv_output_dir, csv_output_sub_dir, pickle_path, username, password, wanted_keys):

    # read pickle
    with open(pickle_path, 'rb') as handle:
        prop_name_dict = pickle.load(handle)

    # read tcf json
    df_tcf = gpd.read_file(tcf_geojson_fpath)

    # make directories for each property
    for i in df_tcf.Project:
        if not os.path.exists(os.path.join(csv_output_sub_dir, i)):
            os.mkdir(os.path.join(csv_output_sub_dir, i))

    print(prop_name_dict)
    for csv_file_property in glob.glob(input_csv_boundaries_dir+"/*.csv"):
        prop_name = prop_name_dict[os.path.basename(csv_file_property)]
        print("Downloading Property: ", prop_name)
        download_gedifiles(df_tcf, csv_file_property, prop_name, username, password, h5_output_dir, csv_output_sub_dir, csv_output_dir, wanted_keys)


def County_DFs(prop_gjson, df_property, output_h5_csv_sub):
    """
    Input:
    prop_gjson = geojson of property of interest
    df_property = geopandas dataframe containing shapely formatted points of interest
    file_name = output file name
    Output:
    .csv with shapely points inside the prop_gjson multipolygon
    """
    df_property['geometry'] = df_property.apply(lambda row: Point(row.lon_lowestmode, row.lat_lowestmode), axis=1) # Pretty expensive operation
    # Make sure projected the same
    df_property = gpd.GeoDataFrame(df_property)
    df_property = df_property.set_crs("EPSG:4326")
    df_explode = prop_gjson.explode().set_crs("EPSG:4326", allow_override = True)
    # Spatial Join
    joinDF = gpd.sjoin(df_property, df_explode, how="inner", op="within")
    print(output_h5_csv_sub, len(joinDF))
    # To csv
    joinDF.to_csv(output_h5_csv_sub)
    return
