#!/usr/bin/env python
# coding: utf-8

# # Extracting Raster Data

# **IMPORTANT**
#
# <ins>Change CRS</ins>
#
# Rasters must be converted into the proper CRS (WGS84/ESPG4326) with the following terminal command:
#
# `gdalwarp input.(img/tiff) output.(img/tiff) -t_srs "+proj=longlat +ellps=WGS84"`
#
# <ins>Convert img to tiff</ins>
#
# Tiff files are used due to their speed.
#
# `gdal_translate -of GTiff input.img output.tiff`

# In[128]:


# Raster 1
# Conus Forest Type (1992)
# https://data.fs.usda.gov/geodata/rastergateway/forest_type/conus_forest-type.zip

# Raster 2
# Conus Forest Group (1992)
# https://data.fs.usda.gov/geodata/rastergateway/forest_type/conus_forestgroup.zip

# Raster 3
# Tree Canopy Cover (2016)
# https://data.fs.usda.gov/geodata/rastergateway/treecanopycover/docs/usfs_carto_CONUS_2016.zip

# Raster 4
# Forest Biomass (1992)
# https://data.fs.usda.gov/geodata/rastergateway/biomass/conus_forest_biomass.zip

# Raster 5
# Biomass carbon stock (prediction)
# https://www.usgs.gov/apps/landcarbon/categories/biomass-c/download/

# Raster 6
# Historical Preciptation
# https://www.fs.fed.us/rm/boise/AWAE/projects/NFS-regional-climate-change-maps/categories/us-raster-layers.html

# Raster 7
# Historical Temperature
# https://data.fs.usda.gov/geodata/rastergateway/OSC/climate.php

# Raster 8
# Forest Land Mask
# https://databasin.org/maps/new/#datasets=4b38d3aad9da4bb593cadc48901e9316


# In[106]:


import rasterio
import numpy as np
from rasterio.plot import show


class IndexableRaster:

    def __init__(self, fpath):
        """
        Input: Raster file path in tiff format with CRS: WGS84/ESPG4326
        """
        self.raster = rasterio.open(fpath)
        pass

    def getLatLong(self, x, y):
        """
        Input: x and y pixel coordinates
        Output: latitude and longitude in WGS84/ESPG4326
        """
        return self.raster.xy(y, x)

    def getXY(self, lat, long):
        """
        Input: latitude and longitude in WGS84/ESPG4326
        Output: x and y pixel coordinates
        """
        y, x = self.raster.index(lat, long)
        return x, y

    def getRasterValue(self, x, y):
        """
        Input: x and y pixel coordinates
        Output: raster value at given pixel coordinates (float)
        """
        return self.raster.read(1)[y, x]

    def getRasterValueLL(self, lat, long):
        """
        Input: latitude and longitude in WGS84/ESPG4326
        Output: raster value at given latitude and longitude (WGS84/EPSG4326)
        """
        return self.getRasterValue(*self.getXY(lat, long))

    def showRaster(self):
        """
        Visualize raster
        """
        show(self.raster)
        pass
