import requests
import os
import pandas as pd
import geopandas as gpd
import pickle
import warnings
warnings.filterwarnings("ignore")

prop_name_dict = {}
GEDI_BASE_URL = 'https://lpdaacsvc.cr.usgs.gov/services/gedifinder?'

def get_request_url(bbox_coordinates):
    assert isinstance(bbox_coordinates, (list, str))
    base_url = GEDI_BASE_URL
    request_params = {"product": "GEDI02_A", "bbox": bbox_coordinates, "output": "json"}

    request_url = base_url + "&".join([key + "=" + val for key, val in request_params.items()])
    return request_url


def get_gedi_files(prop_name, request_url):
    r = requests.get(url=request_url)
    data = r.json()

    gedifiles = data['data']
    print('Files found for {}: {}'.format(prop_name, len(gedifiles)))

    return gedifiles


def get_h5_links(tcf_file, pickle_path, output_dir):
    prop = gpd.read_file(tcf_file)
    #print(prop.head())
    #prop1 = prop.loc[prop['Project'] == 'Chadbourne Tree Farm']
    #prop2 = prop.loc[prop['Project'] == 'MN Heritage Forest']

    #prop = pd.concat([prop1, prop2])
    #print(prop.head())
    print('Fetching GEDI file links...')
    prop_list = list(prop['Project'])
    bboxes = []

    for p in prop_list:
        df = prop[prop['Project'] == p]
        bboxes.append(df['geometry'].total_bounds)

    prop['bb'] = bboxes

    prop['bb_r'] = prop['bb']
    for i in prop['bb_r']:
        i[0], i[1] = i[1], i[0]
        i[2], i[3] = i[3], i[2]
        i[0], i[2] = i[2], i[0]

    prop['bb_str'] = prop['bb_r'].astype('str')
    prop['bb_str'] = prop['bb_str'].map(lambda x: x.lstrip('['))
    prop['bb_str'] = prop['bb_str'].map(lambda x: x.rstrip(']'))
    prop['bb_str'] = prop['bb_str'].map(lambda x: x.rstrip(' '))
    prop['bb_str'] = prop['bb_str'].map(lambda x: x.lstrip(' '))

    # Add commas to bounding box string
    prop['bb_str'] = prop['bb_str'].str.replace('  ', ' ')
    prop['bb_str'] = prop['bb_str'].str.replace('  ', ' ')
    prop['bb_str'] = prop['bb_str'].str.replace('  ', ' ')
    prop['bb_str'] = prop['bb_str'].str.replace(' ', ', ')

    prop['req'] = ['']*len(prop['bb_str'])
    for i in range(len(prop['bb_str'])):
        prop['req'][i] = get_request_url(prop['bb_str'][i])

    prop['links'] = ['']*len(prop['req'])
    for i in range(len(prop['req'])):
        prop['links'][i] = get_gedi_files(prop['Project'][i], prop['req'][i])

    prop = pd.DataFrame(prop.drop(columns='geometry'))

    property_list = list(prop['Project'].unique())

    for prop_name in property_list:
        df_subset = prop.loc[prop['Project'] == prop_name]
        prop_csv = prop_name.replace(" ", "").replace("/", "") + ".csv"
        prop_name_dict[prop_csv] = prop_name
        df_subset.to_csv(os.path.join(output_dir, prop_csv))

    with open(pickle_path, 'wb') as handle:
        pickle.dump(prop_name_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)
    print('Finished fetching GEDI file links')
    return prop
