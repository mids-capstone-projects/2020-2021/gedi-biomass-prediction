# gedi-biomass-prediction

NASA’s Global Ecosystem Dynamics Investigation (GEDI) is an orbital LiDAR mission that provides information on Earth’s biosphere. Using manual surveying data from forests in the US, we attempted to use statistical modeling to establish a relationship between GEDI’s 2A data product (metrics on the height of structures on the ground, namely trees) and the aboveground biomass present in forests. Our analysis of the data suggests that the core problem in establishing such a relationship is a lack of spatial overlap between the predictors (GEDI 2A data) and the output variable (manual surveying plots). We present our attempt to solve this problem using a nearest neighbor interpolation approach.

![GEDI Shots Kepler](./media/gedi_shots.png)

GEDI Shots and Manual Surveying Data. _Source: The Conservation Fund_


# Instructions

1. Download required packages by running the following in the main directory.

`pip install -r requirements.txt`

2. Alter the `config.json` file to specify the following:
    1. Your [NASA EarthData](https://urs.earthdata.nasa.gov/) login details.
    2. Filepath to GeoJSON with shapes polygons around areas on interest.


3. Run the `gedi_download.py` file to begin the downloading of data. 

`python3 gedi_downlaod.py`

Additionally, there are two arguments you may pass with this script. 

`-c any_config.json`: if you wish to specify a configuration file

`-r`: to restart an ongoing download 


4. Open and run `Modeling_MLP.ipynb` to run model the results.

# Preparation of Rasters

<ins>Change CRS</ins>

Rasters must be converted into the proper CRS (WGS84/ESPG4326) with the following terminal command:

`gdalwarp input.(img/tiff) output.(img/tiff) -t_srs "+proj=longlat +ellps=WGS84"`

<ins>Convert img to tiff</ins>

Tiff files are used due to their speed.

`gdal_translate -of GTiff input.img output.tiff`

# Contributors
* Ravitashaw Bathla
* Ethan Swartzentruber
* Nathan Warren
