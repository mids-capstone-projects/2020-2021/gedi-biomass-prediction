import pandas as pd
import glob
from lib.standardize_rasters_io import IndexableRaster
import lib.config as constants
import os


def merge_inventory_rasters(inventory_data_dir):
    """
    Merge all raster data with TCF manual surveying sites
    """
    # Get all survey files
    all_survey_files = [i for i in glob.glob(
        os.path.join(inventory_data_dir, '*.csv'))]

    # File path for rasters
    #canopy_cover = 'rasters/tree_canopy_reproj.tiff' # This raster takes a lot of processing power
    rasters = constants['RASTERS'].values()

    # Concatenate into a single dataframe
    inventory = pd.concat([pd.read_csv(f) for f in all_survey_files])
    inventory[['Latitude', 'Longitude']] = inventory[[
        'Latitude', 'Longitude']].astype('float64')

    # Insert names for each raster, will be used to label columns in inventory_with_rasters.csv
    # Length of this list should equal the length of the RASTERS list above
    raster_col_names = constants['RASTERS'].keys()

    # Create objects for each raster
    for i in range(len(rasters)):
        rasters[i] = IndexableRaster(rasters[i])

    # Extract Raster data and add to inventory dataframe
    for i in range(len(rasters)):
        inventory[raster_col_names[i]] = rasters[i].getRasterValueLL(
            inventory['Longitude'], inventory['Latitude'])

    inventory.to_csv("inventory_with_rasters.csv", index=False)


if __name__ == "__main__":
    inventory_data_dir = constants['INVENTORY_DATA_DIR']
    print('Started: Matching raster data to inventory data')
    merge_inventory_rasters(inventory_data_dir)
    print('Finished: Matching raster data to inventory data')
