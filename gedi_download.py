import os
from glob import glob

import typer
import lib.config as cfg
from lib.download_h5_list import get_h5_links
from lib.download_h5_subset import download
from lib.merge_l2a_data import merge_l2a

def clean_temp_dirs(h5_output_dir, csv_output_dir, subsetted_output_dir):
    for h5_file in glob(f"{h5_output_dir}/*.h5"):
        os.remove(h5_file)
    for csv_file in glob(f"{csv_output_dir}/*.csv"):
        os.remove(csv_file)
    output_files = glob(f"{subsetted_output_dir}/*/*.csv")
    if len(output_files) > 0:
        latest_file = max(output_files, key=os.path.getctime)
        os.remove(latest_file)


def create_if_not_exists(dir_path):
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)


def create_temp_dirs(constants):
    create_if_not_exists("./tmp")
    create_if_not_exists(constants['H5_SOURCE_DIR'])
    create_if_not_exists(constants['H5_CSV_DIR'])
    create_if_not_exists(constants['TCF_PROPERTY_CSV_DIR'])
    create_if_not_exists(constants['H5_CSV_SUBSET_DIR'])


# CLI entypoint
def main(
    config: str = typer.Option(..., "-c", help="Config file"),
    resume: bool = typer.Option(False, "-r", help="Restart an ongoing download with its state saved in ./tmp")
):
    """
    Download GEDI L2A data for boundaries given in boundaries_geojson.
    """
    constants = cfg.load_config(config)
    create_temp_dirs(constants)

    # download_h5_list
    pickle_path = constants['TCF_PROPERTY_PICKLE_FILE']
    tcf_geojson_fpath = constants['TCF_PROPERTY_GEOJSON_FILE']
    output_dir = constants['TCF_PROPERTY_CSV_DIR']

    get_h5_links(tcf_geojson_fpath, pickle_path, output_dir)

    # download_h5_subset
    tcf_geojson_fpath = constants['TCF_PROPERTY_GEOJSON_FILE']
    pickle_path = constants['TCF_PROPERTY_PICKLE_FILE']
    input_csv_boundaries_dir = constants['TCF_PROPERTY_CSV_DIR']
    h5_output_dir = constants['H5_SOURCE_DIR']
    csv_output_dir = constants['H5_CSV_DIR']
    csv_output_sub_dir = constants['H5_CSV_SUBSET_DIR']
    username = constants['GEDI_USERNAME']
    password = constants['GEDI_PASSWORD']
    wanted_keys = constants['GEDI_L2A_KEYS']

    if resume:
        print("Cleaning temp directories and resuming.")
        clean_temp_dirs(h5_output_dir, csv_output_dir, csv_output_sub_dir)

    download(tcf_geojson_fpath, input_csv_boundaries_dir, h5_output_dir, csv_output_dir, csv_output_sub_dir, pickle_path, username, password, wanted_keys)
    print('Download Finished')

    print("Merging L2A Data")
    tcf_geojson_fpath = constants['TCF_PROPERTY_GEOJSON_FILE']
    l2a_csv_dirname = constants['H5_CSV_SUBSET_DIR']
    output_l2a_file = constants['L2A_TCF_OUTPATH']

    merge_l2a(tcf_geojson_fpath, l2a_csv_dirname, output_l2a_file)
    print('Finished: L2A Merge')


if __name__ == "__main__":
    typer.run(main)
